<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::create([
            'name' => 'Admin'
        ]);
        $roleUser = Role::create([
            'name' => 'User'
        ]);

        $user1 = User::create([
            'name'     =>'Yosef',
            'email'    =>'yosef@bncc.net',
            'password' =>bcrypt('yosefganteng'),
            'age'      =>25,
            'rating'   =>10,
            'hobby'    =>'dance',
        ]);

        $user2 = User::create([
            'name'     =>'Rais',
            'email'    =>'rais@bncc.net',
            'password' =>bcrypt('raisganteng'),
            'age'      =>15,
            'rating'   =>3,
            'hobby'    =>'cooking',
        ]);

        $userAdmin = User::create([
            'name'     =>'Admin',
            'email'    =>'admin@bncc.net',
            'password' => bcrypt('adminadmin'),
            'age'      =>999,
            'rating'   =>999,
            'hobby'    =>'admin',
        ]);
        
        $userAdmin->roles()->attach($roleAdmin->id);
        $user1->roles()->attach($roleUser->id);
        $user2->roles()->attach($roleUser->id);
    }
}
